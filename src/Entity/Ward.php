<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\WardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WardRepository::class)
 * @ORM\Table(name="ward")
 */
class Ward
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $number;

    /**
     * @ORM\Column(type="integer")
     */
    private int $beds;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Medic", inversedBy="wards")
     * @ORM\JoinColumn(name="medic", referencedColumnName="id")
     */
    private Medic $medic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="wards")
     * @ORM\JoinColumn(name="patient", referencedColumnName="id")
     */
    private Patient $patient;

    public function getId(): int
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getBeds(): int
    {
        return $this->beds;
    }

    public function setBeds(int $beds): self
    {
        $this->beds = $beds;

        return $this;
    }

    public function getMedic(): Medic
    {
        return $this->medic;
    }

    public function setMedic(Medic $medic): self
    {
        $this->medic = $medic;

        return $this;
    }

    public function getPatient(): Patient
    {
        return $this->patient;
    }

    public function setPatient(Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
