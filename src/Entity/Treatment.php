<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TreatmentRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TreatmentRepository::class)
 * @ORM\Table(name="treatment")
 */
class Treatment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(name="dateOfStart", type="time")
     */
    private DateTime $dateOfStart;

    /**
     * @ORM\Column(name="dateOfEnd", type="time")
     */
    private DateTime $dateOfEnd;

    /**
     * @ORM\Column(type="text")
     */
    private string $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Medic", inversedBy="treatments")
     * @ORM\JoinColumn(name="medic", referencedColumnName="id")
     */
    private Medic $medic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="treatments")
     * @ORM\JoinColumn(name="patient", referencedColumnName="id")
     */
    private Patient $patient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateOfStart(): DateTime
    {
        return $this->dateOfStart;
    }

    public function setDateOfStart(DateTime $dateOfStart): self
    {
        $this->dateOfStart = $dateOfStart;

        return $this;
    }

    public function getDateOfEnd(): DateTime
    {
        return $this->dateOfEnd;
    }

    public function setDateOfEnd(DateTime $dateOfEnd): self
    {
        $this->dateOfEnd = $dateOfEnd;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMedic(): Medic
    {
        return $this->medic;
    }

    public function setMedic(Medic $medic): self
    {
        $this->medic = $medic;

        return $this;
    }

    public function getPatient(): Patient
    {
        return $this->patient;
    }

    public function setPatient(Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
