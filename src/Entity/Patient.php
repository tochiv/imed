<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 * @ORM\Table(name="patient")
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $surname;

    /**
     * @ORM\Column(name="middleName", type="string", length=255)
     */
    private string $middleName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="patients")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ward", mappedBy="patient")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $wards;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cabinet", mappedBy="patient")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $cabinets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Laboratory", mappedBy="patient")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $laboratories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Treatment", mappedBy="patient")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $treatments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="patient")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $operations;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->wards = new ArrayCollection();
        $this->cabinets = new ArrayCollection();
        $this->laboratories = new ArrayCollection();
        $this->treatments = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getDisplayName()
    {
        $fullName = "%s" . " %s " . "%s";
        return sprintf($fullName, $this->name, $this->surname, $this->middleName);
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Ward[]
     */
    public function getWards(): Collection
    {
        return $this->wards;
    }

    public function addWard(Ward $ward): self
    {
        if (!$this->wards->contains($ward)) {
            $this->wards[] = $ward;
            $ward->setPatient($this);
        }

        return $this;
    }

    public function removeWard(Ward $ward): self
    {
        if ($this->wards->removeElement($ward)) {
            // set the owning side to null (unless already changed)
            if ($ward->getPatient() === $this) {
                $ward->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cabinet[]
     */
    public function getCabinets(): Collection
    {
        return $this->cabinets;
    }

    public function addCabinet(Cabinet $cabinet): self
    {
        if (!$this->cabinets->contains($cabinet)) {
            $this->cabinets[] = $cabinet;
            $cabinet->setPatient($this);
        }

        return $this;
    }

    public function removeCabinet(Cabinet $cabinet): self
    {
        if ($this->cabinets->removeElement($cabinet)) {
            // set the owning side to null (unless already changed)
            if ($cabinet->getPatient() === $this) {
                $cabinet->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Laboratory[]
     */
    public function getLaboratories(): Collection
    {
        return $this->laboratories;
    }

    public function addLaboratory(Laboratory $laboratory): self
    {
        if (!$this->laboratories->contains($laboratory)) {
            $this->laboratories[] = $laboratory;
            $laboratory->setPatient($this);
        }

        return $this;
    }

    public function removeLaboratory(Laboratory $laboratory): self
    {
        if ($this->laboratories->removeElement($laboratory)) {
            // set the owning side to null (unless already changed)
            if ($laboratory->getPatient() === $this) {
                $laboratory->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Treatment[]
     */
    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function addTreatment(Treatment $treatment): self
    {
        if (!$this->treatments->contains($treatment)) {
            $this->treatments[] = $treatment;
            $treatment->setPatient($this);
        }

        return $this;
    }

    public function removeTreatment(Treatment $treatment): self
    {
        if ($this->treatments->removeElement($treatment)) {
            // set the owning side to null (unless already changed)
            if ($treatment->getPatient() === $this) {
                $treatment->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setPatient($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getPatient() === $this) {
                $operation->setPatient(null);
            }
        }

        return $this;
    }
}
