<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\WorkerRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WorkerRepository::class)
 * @ORM\Table(name="worker")
 */
class Worker
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private string $surname;

    /**
     * @ORM\Column(name="middleName", type="string", length=255)
     */
    private string $middleName;

    /**
     * @ORM\Column(name="avatar", type="string", length=255)
     */
    private string $avatar;

    /**
     * @ORM\Column(name="specialty", type="string", length=255)
     */
    private string $specialty;

    /**
     * @ORM\Column(name="dateOfStart", type="date")
     */
    private DateTime $dateOfStart;

    /**
     * @ORM\Column(name="dateOfEnd", type="date")
     */
    private DateTime $dateOfEnd;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workers")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getSpecialty(): string
    {
        return $this->specialty;
    }

    public function setSpecialty(string $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getDateOfStart(): DateTime
    {
        return $this->dateOfStart;
    }

    public function setDateOfStart(DateTime $dateOfStart): self
    {
        $this->dateOfStart = $dateOfStart;

        return $this;
    }

    public function getDateOfEnd(): DateTime
    {
        return $this->dateOfEnd;
    }

    public function setDateOfEnd(DateTime $dateOfEnd): self
    {
        $this->dateOfEnd = $dateOfEnd;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getDiffDate(DateTime $startDate): string
    {
        return $this->getDateOfEnd()->diff($startDate)->format("%y years %m months");
    }
}
