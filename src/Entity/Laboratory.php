<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\LaboratoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LaboratoryRepository::class)
 * @ORM\Table(name="laboratory")
 */
class Laboratory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="nameOfResearch", type="string", length=255)
     */
    private string $nameOfResearch;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private string $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="laboratories")
     * @ORM\JoinColumn(name="patient", referencedColumnName="id")
     */
    private Patient $patient;

    public function getId(): int
    {
        return $this->id;
    }

    public function getNameOfResearch(): ?string
    {
        return $this->nameOfResearch;
    }

    public function setNameOfResearch(string $nameOfResearch): self
    {
        $this->nameOfResearch = $nameOfResearch;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPatient(): Patient
    {
        return $this->patient;
    }

    public function setPatient(Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }
}
