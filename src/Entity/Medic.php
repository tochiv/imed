<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MedicRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MedicRepository::class)
 * @ORM\Table(name="medic")
 */
class Medic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private string $surname;

    /**
     * @ORM\Column(name="middleName", type="string", length=255)
     */
    private string $middleName;

    /**
     * @ORM\Column(name="specialty", type="string", length=255)
     */
    private string $specialty;

    /**
     * @ORM\Column(name="startDate", type="date", length=255)
     */
    private \DateTime $startDate;

    /**
     * @ORM\Column(name="endDate", type="date", length=255)
     */
    private \DateTime $endDate;

    /**
     * @ORM\Column(name="skillLevel", type="string", length=255)
     */
    private string $skillLevel;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(name="productivity", type="integer")
     */
    private int $productivity;

    /**
     * @ORM\Column(name="avatar", type="string", length=255)
     */
    private string $avatar;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="medics")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ward", mappedBy="medic")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $wards;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cabinet", mappedBy="medic")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $cabinets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Treatment", mappedBy="medic")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $treatments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="medic")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Collection $operations;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->wards = new ArrayCollection();
        $this->cabinets = new ArrayCollection();
        $this->treatments = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getDisplayName(): string
    {
        $fullName = "%s" . " %s " . "%s";
        return sprintf($fullName, $this->name, $this->surname, $this->middleName);
    }

    public function getSpecialty(): ?string
    {
        return $this->specialty;
    }

    public function setSpecialty(string $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getSkillLevel(): ?string
    {
        return $this->skillLevel;
    }

    public function setSkillLevel(string $skillLevel): self
    {
        $this->skillLevel = $skillLevel;

        return $this;
    }

    public function getProductivity(): ?int
    {
        return $this->productivity;
    }

    public function setProductivity(int $productivity): self
    {
        $this->productivity = $productivity;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getDiffDate(DateTime $startDate): string
    {
        return $this->getEndDate()->diff($startDate)->format("%y years %m months");
    }


    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Ward[]
     */
    public function getWards(): Collection
    {
        return $this->wards;
    }

    public function addWard(Ward $ward): self
    {
        if (!$this->wards->contains($ward)) {
            $this->wards[] = $ward;
            $ward->setPatient($this);
        }

        return $this;
    }

    public function removeWard(Ward $ward): self
    {
        if ($this->wards->removeElement($ward)) {
            // set the owning side to null (unless already changed)
            if ($ward->getPatient() === $this) {
                $ward->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cabinet[]
     */
    public function getCabinets(): Collection
    {
        return $this->cabinets;
    }

    public function addCabinet(Cabinet $cabinet): self
    {
        if (!$this->cabinets->contains($cabinet)) {
            $this->cabinets[] = $cabinet;
            $cabinet->setPatient($this);
        }

        return $this;
    }

    public function removeCabinet(Cabinet $cabinet): self
    {
        if ($this->cabinets->removeElement($cabinet)) {
            // set the owning side to null (unless already changed)
            if ($cabinet->getPatient() === $this) {
                $cabinet->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Treatment[]
     */
    public function getTreatments(): Collection
    {
        return $this->treatments;
    }

    public function addTreatment(Treatment $treatment): self
    {
        if (!$this->treatments->contains($treatment)) {
            $this->treatments[] = $treatment;
            $treatment->setPatient($this);
        }

        return $this;
    }

    public function removeTreatment(Treatment $treatment): self
    {
        if ($this->treatments->removeElement($treatment)) {
            // set the owning side to null (unless already changed)
            if ($treatment->getPatient() === $this) {
                $treatment->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setPatient($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getPatient() === $this) {
                $operation->setPatient(null);
            }
        }

        return $this;
    }
}
