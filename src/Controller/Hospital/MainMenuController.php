<?php

declare(strict_types=1);

namespace App\Controller\Hospital;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainMenuController extends AbstractController
{
    /**
     * @Route("/", name="app_mainMenu")
     */
    public function index(): Response
    {
        return $this->render('hospital/index.html.twig',
            []
        );
    }
}
