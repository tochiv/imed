<?php

declare(strict_types=1);

namespace App\Controller\Hospital;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HospitalControlController extends AbstractController
{
    /**
     * @Route("/hospital", name="app_hospitalControl")
     */
    public function index(): Response
    {
        return $this->render('hospitalControl/index.html.twig', [
            'controller_name' => 'HospitalControlController',
        ]);
    }
}
