<?php

declare(strict_types=1);

namespace App\Controller\Patient;

use App\Entity\Patient;
use App\Entity\User;
use App\Form\PatientFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddPatientController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/patient/add", name="app_addPatient")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $patient = new Patient($user);

        $form = $this->createForm(PatientFormType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($patient);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_patientControl');
        }

        return $this->render('patient/add.html.twig', [
            'patientForm' => $form->createView()
        ]);
    }
}
