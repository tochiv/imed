<?php

declare(strict_types=1);

namespace App\Controller\Patient;

use App\Entity\Patient;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PatientControlController extends AbstractController
{
    /**
     * @Route("/patient", name="app_patientControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Patient $patients */
        $patients = $user->getPatients();

        return $this->render('patient/show.html.twig', [
            'patients' => $patients
        ]);
    }
}
