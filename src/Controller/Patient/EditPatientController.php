<?php

declare(strict_types=1);

namespace App\Controller\Patient;

use App\Entity\Patient;
use App\Form\PatientFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditPatientController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/patient/edit", name="app_editPatient")
     */
    public function edit(Request $request, Patient $patient): Response
    {
        $form = $this->createForm(PatientFormType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_patientControl');
        }

        return $this->render('patient/edit.html.twig', [
            'patient' => $patient,
            'patientForm' => $form->createView()
        ]);
    }
}
