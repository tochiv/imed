<?php

declare(strict_types=1);

namespace App\Controller\Operation;

use App\Entity\Operation;
use App\Entity\User;
use App\Repository\OperationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OperationControlController extends AbstractController
{
    public OperationRepository $operationRepository;

    public function __construct(OperationRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }

    /**
     * @Route("/operation", name="app_operationControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();;
        /** @var Operation $operations */
        $operations = $this->operationRepository->getOperations($user);

        return $this->render('operation/show.html.twig', [
            'operations' => $operations
        ]);
    }
}
