<?php

declare(strict_types=1);

namespace App\Controller\Operation;

use App\Entity\Operation;
use App\Form\OperationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditOperationController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/operation/edit", name="app_editOperation")
     */
    public function edit(Request $request, Operation $operation): Response
    {
        $form = $this->createForm(OperationFormType::class, $operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_operationControl');
        }

        return $this->render('operation/edit.html.twig', [
            'operation' => $operation,
            'operationForm' => $form->createView()
        ]);
    }
}
