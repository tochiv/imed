<?php

declare(strict_types=1);

namespace App\Controller\Operation;

use App\Entity\Operation;
use App\Entity\User;
use App\Form\OperationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddOperationController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/operation/add", name="app_addOperation")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $operation = new Operation();

        $form = $this->createForm(OperationFormType::class, $operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $this->entityManager->persist($operation);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_operationControl');
        }

        return $this->render('operation/add.html.twig', [
            'operationForm' => $form->createView()
        ]);
    }
}
