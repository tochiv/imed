<?php

declare(strict_types=1);

namespace App\Controller\Worker;

use App\Entity\Worker;
use App\Form\WardFormType;
use App\Form\WorkerFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditWorkerController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/worker/edit", name="app_editWorker")
     */
    public function edit(Request $request, Worker $worker): Response
    {
        $form = $this->createForm(WorkerFormType::class, $worker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_workerControl');
        }

        return $this->render('worker/edit.html.twig', [
            'worker' => $worker,
            'workerForm' => $form->createView()
        ]);
    }
}
