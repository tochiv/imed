<?php

declare(strict_types=1);

namespace App\Controller\Worker;

use App\Entity\User;
use App\Entity\Worker;
use App\Form\WorkerFormType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddWorkerController extends AbstractController
{
    private FileUploader $fileUploader;
    private EntityManagerInterface $entityManager;

    public function __construct(FileUploader $fileUploader, EntityManagerInterface $entityManager)
    {
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/worker/add", name="app_AddWorker")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $worker = new Worker($user);

        $form = $this->createForm(WorkerFormType::class, $worker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avatarFile = $form->get('avatar')->getData();

            if ($avatarFile){
                $avatarFileName = $this->fileUploader->upload($avatarFile);
                $worker->setAvatar($avatarFileName);
            }

            $this->entityManager->persist($worker);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_workerControl');
        }

        return $this->render('worker/add.html.twig', [
            'workerForm' => $form->createView()
        ]);
    }
}
