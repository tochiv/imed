<?php

declare(strict_types=1);

namespace App\Controller\Worker;

use App\Entity\User;
use App\Entity\Worker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WorkerControlController extends AbstractController
{
    /**
     * @Route("/worker", name="app_workerControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Worker $worker */
        $workers = $user->getWorkers();

        return $this->render('worker/show.html.twig', [
            'workers' => $workers,
        ]);
    }
}
