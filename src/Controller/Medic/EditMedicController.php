<?php

declare(strict_types=1);

namespace App\Controller\Medic;

use App\Entity\Medic;
use App\Form\MedicFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditMedicController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/medic/edit", name="app_editMedic")
     */
    public function edit(Request $request, Medic $medic): Response
    {
        $form = $this->createForm(MedicFormType::class, $medic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_medicControl');
        }

        return $this->render('medic/edit.html.twig', [
            'medic' => $medic,
            'medicForm' => $form->createView()
        ]);
    }
}
