<?php

declare(strict_types=1);

namespace App\Controller\Medic;

use App\Entity\Medic;
use App\Entity\User;
use App\Form\MedicFormType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddMedicController extends AbstractController
{
    private FileUploader $fileUploader;
    private EntityManagerInterface $entityManager;

    public function __construct(FileUploader $fileUploader, EntityManagerInterface $entityManager)
    {
        $this->fileUploader = $fileUploader;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/medic/add", name="app_addMedic")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $medic = new Medic($user);

        $form = $this->createForm(MedicFormType::class, $medic);
        $form->handleRequest($request);

        if ($form->isSubmitted()  && $form->isValid()){
            $avatarFile = $form->get('avatar')->getData();
            if ($avatarFile){
                $avatarFileName = $this->fileUploader->upload($avatarFile);
                $medic->setAvatar($avatarFileName);
            }

            $this->entityManager->persist($medic);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_medicControl');
        }

        return $this->render('medic/add.html.twig', [
            'medicForm' => $form->createView()
        ]);
    }
}
