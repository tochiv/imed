<?php

declare(strict_types=1);

namespace App\Controller\Medic;

use App\Entity\Medic;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MedicControlController extends AbstractController
{
    /**
     * @Route("/medic", name="app_medicControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Medic $medics */
        $medics = $user->getMedics();

        return $this->render('medic/show.html.twig', [
            "medics" => $medics,
        ]);
    }
}
