<?php

declare(strict_types=1);


namespace App\Controller\Ward;

use App\Entity\User;
use App\Entity\Ward;
use App\Form\WardFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddWardController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/ward/add", name="app_addWard")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $ward = new Ward();

        $form = $this->createForm(WardFormType::class, $ward);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $this->entityManager->persist($ward);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_wardControl');
        }

        return $this->render('ward/add.html.twig', [
            'wardForm' => $form->createView(),
        ]);
    }
}
