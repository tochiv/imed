<?php

declare(strict_types=1);

namespace App\Controller\Ward;

use App\Entity\Ward;
use App\Form\WardFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditWardController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/ward/edit", name="app_editWard")
     */
    public function edit(Request $request, Ward $ward): Response
    {
        $form = $this->createForm(WardFormType::class, $ward);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_wardControl');
        }

        return $this->render('ward/edit.html.twig', [
            'ward' => $ward,
            'wardForm' => $form->createView()
        ]);
    }
}
