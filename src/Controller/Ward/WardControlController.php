<?php

declare(strict_types=1);

namespace App\Controller\Ward;

use App\Entity\User;
use App\Repository\WardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WardControlController extends AbstractController
{
    public WardRepository $wardRepository;

    public function __construct(WardRepository $wardRepository)
    {
        $this->wardRepository = $wardRepository;
    }

    /**
     * @Route("/ward", name="app_wardControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $wards = $this->wardRepository->getWards($user);


        return $this->render('ward/show.html.twig', [
            'wards' => $wards,
        ]);
    }
}
