<?php

declare(strict_types=1);

namespace App\Controller\Laboratory;

use App\Entity\Laboratory;
use App\Form\LaboratoryFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditLaboratoryController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/laboratory/edit", name="app_editLaboratory")
     */
    public function edit(Request $request, Laboratory $laboratory): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(LaboratoryFormType::class, $laboratory, ['user' => $user]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_laboratoryControl');
        }

        return $this->render('laboratory/edit.html.twig', [
            'laboratory' => $laboratory,
            'laboratoryForm' => $form->createView()
        ]);
    }
}
