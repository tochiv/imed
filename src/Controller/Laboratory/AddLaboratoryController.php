<?php

declare(strict_types=1);

namespace App\Controller\Laboratory;

use App\Entity\Laboratory;
use App\Entity\User;
use App\Form\LaboratoryFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddLaboratoryController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/laboratory/add", name="app_addLaboratory")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $laboratory = new Laboratory();

        $form = $this->createForm(LaboratoryFormType::class, $laboratory, [
            'user' => $user
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->entityManager->persist($laboratory);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_laboratoryControl');
        }

        return $this->render('laboratory/add.html.twig', [
            'laboratoryForm' => $form->createView()
        ]);
    }
}
