<?php

declare(strict_types=1);

namespace App\Controller\Laboratory;

use App\Entity\User;
use App\Repository\LaboratoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LaboratoryControlController extends AbstractController
{
    public LaboratoryRepository $laboratoryRepository;

    public function __construct(LaboratoryRepository $laboratoryRepository)
    {
        $this->laboratoryRepository = $laboratoryRepository;
    }

    /**
     * @Route("/laboratory", name="app_laboratoryControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $laboratories = $this->laboratoryRepository->getLaboratory($user);

        return $this->render('laboratory/show.html.twig', [
            'laboratories' => $laboratories,
        ]);
    }
}
