<?php

declare(strict_types=1);

namespace App\Controller\Treatment;

use App\Entity\Treatment;
use App\Entity\User;
use App\Form\TreatmentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddTreatmentController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/treatment/add", name="app_addTreatment")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $treatment = new Treatment();

        $form = $this->createForm(TreatmentFormType::class, $treatment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($treatment);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_treatmentControl');
        }

        return $this->render('treatment/add.html.twig', [
            'treatmentForm' => $form->createView()
        ]);
    }
}
