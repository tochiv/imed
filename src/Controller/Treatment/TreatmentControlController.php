<?php

declare(strict_types=1);

namespace App\Controller\Treatment;

use App\Entity\Treatment;
use App\Entity\User;
use App\Repository\TreatmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TreatmentControlController extends AbstractController
{
    public TreatmentRepository $treatmentRepository;

    public function __construct(TreatmentRepository $treatmentRepository)
    {
        $this->treatmentRepository = $treatmentRepository;
    }

    /**
     * @Route("/treatment", name="app_treatmentControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Treatment $treatments */
        $treatments = $this->treatmentRepository->getTreatments($user);

        return $this->render('treatment/show.html.twig', [
            'treatments' => $treatments,
        ]);
    }
}
