<?php

declare(strict_types=1);

namespace App\Controller\Treatment;

use App\Entity\Treatment;
use App\Form\TreatmentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditTreatmentController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/treatment/edit", name="app_editTreatment")
     */
    public function edit(Request $request, Treatment $treatment): Response
    {
        $form = $this->createForm(TreatmentFormType::class, $treatment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_treatmentControl');
        }

        return $this->render('treatment/edit.html.twig', [
            'treatment' => $treatment,
            'treatmentForm' => $form->createView()
        ]);
    }
}
