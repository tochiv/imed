<?php

declare(strict_types=1);

namespace App\Controller\Cabinet;

use App\Entity\Cabinet;
use App\Entity\User;
use App\Form\CabinetFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddCabinetController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/cabinet/add", name="app_addCabinet")
     */
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $cabinet = new Cabinet();

        $form = $this->createForm(CabinetFormType::class, $cabinet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($cabinet);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_cabinetControl');
        }

        return $this->render('cabinet/add.html.twig', [
            'cabinetForm' => $form->createView()
        ]);
    }
}
