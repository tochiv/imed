<?php

namespace App\Controller\Cabinet;

use App\Entity\Cabinet;
use App\Form\CabinetFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditCabinetController extends AbstractController
{
    public EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/cabinet/edit", name="app_editCabinet")
     */
    public function edit(Request $request, Cabinet $cabinet): Response
    {
        $form = $this->createForm(CabinetFormType::class, $cabinet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_cabinetControl');
        }

        return $this->render('cabinet/edit.html.twig', [
            'cabinet' => $cabinet,
            'cabinetForm' => $form->createView()
        ]);
    }
}
