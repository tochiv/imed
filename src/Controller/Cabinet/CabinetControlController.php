<?php

declare(strict_types=1);

namespace App\Controller\Cabinet;

use App\Entity\User;
use App\Repository\CabinetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CabinetControlController extends AbstractController
{
    public CabinetRepository $cabinetLaboratory;

    public function __construct(CabinetRepository $cabinetLaboratory)
    {
        $this->cabinetLaboratory = $cabinetLaboratory;
    }

    /**
     * @Route("/cabinet", name="app_cabinetControl")
     */
    public function index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $cabinets = $this->cabinetLaboratory->getMedics($user);

        return $this->render('cabinet/show.html.twig', [
            'cabinets' => $cabinets
        ]);
    }
}
