<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Cabinet;
use App\Entity\Medic;
use App\Entity\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CabinetFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', NumberType::class, [
                    'label' => 'Number of Cabinet'
                ]
            )
            ->add('medic', EntityType::class, [
                    'class' => Medic::class,
                    'choice_label' => function ($medic) {
                        return $medic->getDisplayName();
                    }
                ]
            )
            ->add('patient', EntityType::class, [
                    'class' => Patient::class,
                    'choice_label' => function ($patient) {
                        return $patient->getDisplayName();
                    }
                ]
            )
            ->add('btn', SubmitType::class, [
                    'label' => 'Add'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cabinet::class,
        ]);
    }
}
