<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Laboratory;
use App\Entity\Patient;
use App\Entity\User;
use App\Repository\LaboratoryRepository;
use App\Repository\PatientRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LaboratoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $options['user'];
        $builder
            ->add('nameOfResearch', TextType::class, [
                    'label' => 'Name of research'
                ]
            )
            ->add('description', TextareaType::class, [
                    'label' => 'Description of research'
                ]
            )
            ->add('patient', EntityType::class, [
                    'class' => Patient::class,
                    'choice_label' => function($patient) {
                        return $patient->getDisplayName();
                    },
                ]
            )
            ->add('btn', SubmitType::class, [
                    'label' => 'Add'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Laboratory::class,
        ]);
        $resolver->setRequired('user');
        $resolver->setAllowedTypes('user', [User::class]);
    }
}
