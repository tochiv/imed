<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Worker;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class WorkerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                    'label' => 'Name'
                ]
            )
            ->add('surname', TextType::class, [
                    'label' => 'Surname'
                ]
            )
            ->add('middleName', TextType::class, [
                    'label' => 'Middle Name'
                ]
            )
            ->add('avatar', FileType::class, [
                    'label' => 'Avatar',
                    'mapped' => false,
                    'required' => false,
                    'constraints' => [
                        new File([
                            'maxSize' => '2048k',
                            'mimeTypes' => [
                                'image/png',
                                'image/jpeg',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid IMG'
                        ])
                    ]
                ]
            )
            ->add('specialty', ChoiceType::class, [
                    'label' => 'Specialty',
                    'choices' => [
                        'Nurse' => 'nurse',
                        'Orderly' => 'orderly',
                        'Cleaner' => 'cleaner'
                    ]
                ]
            )
            ->add('dateOfStart', DateType::class, [
                    'label' => 'Start date of work',
                    'placeholder' => [
                        'year' => 'Year',
                        'month' => 'Month',
                        'day' => 'Day',
                    ]
                ]
            )
            ->add('dateOfEnd', DateType::class, [
                    'label' => 'End date of work',
                    'placeholder' => [
                        'year' => 'Year',
                        'month' => 'Month',
                        'day' => 'Day',
                    ]
                ]
            )
            ->add('btn', SubmitType::class, [
                    'label' => 'Add'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Worker::class,
        ]);
    }
}
