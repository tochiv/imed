<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Medic;
use App\Entity\Operation;

use App\Entity\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                    'label' => 'Name of Operation'
                ]
            )
            ->add('description', TextareaType::class, [
                    'label' => 'Description'
                ]
            )
            ->add('medic', EntityType::class, [
                    'class' => Medic::class,
                    'choice_label' => function ($medic) {
                        return $medic->getDisplayName();
                    }
                ]
            )
            ->add('patient', EntityType::class, [
                    'class' => Patient::class,
                    'choice_label' => function ($patient) {
                        return $patient->getDisplayName();
                    }
                ]
            )
            ->add('btn', SubmitType::class, [
                    'label' => 'Add'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Operation::class,
        ]);
    }
}
