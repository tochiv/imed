<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505081219 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66D8D93D649');
        $this->addSql('DROP INDEX IDX_1981A66D8D93D649 ON operation');
        $this->addSql('ALTER TABLE operation DROP user');
        $this->addSql('ALTER TABLE treatment DROP FOREIGN KEY FK_98013C318D93D649');
        $this->addSql('DROP INDEX IDX_98013C318D93D649 ON treatment');
        $this->addSql('ALTER TABLE treatment DROP user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D8D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1981A66D8D93D649 ON operation (user)');
        $this->addSql('ALTER TABLE treatment ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE treatment ADD CONSTRAINT FK_98013C318D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_98013C318D93D649 ON treatment (user)');
    }
}
