<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505091026 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet DROP FOREIGN KEY FK_4CED05B06B899279');
        $this->addSql('DROP INDEX IDX_4CED05B06B899279 ON cabinet');
        $this->addSql('ALTER TABLE cabinet CHANGE patient_id patient INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cabinet ADD CONSTRAINT FK_4CED05B01ADAD7EB FOREIGN KEY (patient) REFERENCES patient (id)');
        $this->addSql('CREATE INDEX IDX_4CED05B01ADAD7EB ON cabinet (patient)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet DROP FOREIGN KEY FK_4CED05B01ADAD7EB');
        $this->addSql('DROP INDEX IDX_4CED05B01ADAD7EB ON cabinet');
        $this->addSql('ALTER TABLE cabinet CHANGE patient patient_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cabinet ADD CONSTRAINT FK_4CED05B06B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_4CED05B06B899279 ON cabinet (patient_id)');
    }
}
