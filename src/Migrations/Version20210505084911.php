<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505084911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet DROP FOREIGN KEY FK_4CED05B08422C020');
        $this->addSql('DROP INDEX IDX_4CED05B08422C020 ON cabinet');
        $this->addSql('ALTER TABLE cabinet DROP medic');
        $this->addSql('ALTER TABLE laboratory DROP FOREIGN KEY FK_FDC719A81ADAD7EB');
        $this->addSql('DROP INDEX IDX_FDC719A81ADAD7EB ON laboratory');
        $this->addSql('ALTER TABLE laboratory DROP patient');
        $this->addSql('ALTER TABLE medic ADD ward INT DEFAULT NULL, ADD cabinet INT DEFAULT NULL');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C020C96F581B FOREIGN KEY (ward) REFERENCES ward (id)');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C0204CED05B0 FOREIGN KEY (cabinet) REFERENCES cabinet (id)');
        $this->addSql('CREATE INDEX IDX_8422C020C96F581B ON medic (ward)');
        $this->addSql('CREATE INDEX IDX_8422C0204CED05B0 ON medic (cabinet)');
        $this->addSql('ALTER TABLE patient ADD ward INT DEFAULT NULL, ADD cabinet INT DEFAULT NULL, ADD laboratory INT DEFAULT NULL');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBC96F581B FOREIGN KEY (ward) REFERENCES ward (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB4CED05B0 FOREIGN KEY (cabinet) REFERENCES cabinet (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBFDC719A8 FOREIGN KEY (laboratory) REFERENCES laboratory (id)');
        $this->addSql('CREATE INDEX IDX_1ADAD7EBC96F581B ON patient (ward)');
        $this->addSql('CREATE INDEX IDX_1ADAD7EB4CED05B0 ON patient (cabinet)');
        $this->addSql('CREATE INDEX IDX_1ADAD7EBFDC719A8 ON patient (laboratory)');
        $this->addSql('ALTER TABLE ward DROP FOREIGN KEY FK_C96F581B1ADAD7EB');
        $this->addSql('ALTER TABLE ward DROP FOREIGN KEY FK_C96F581B8422C020');
        $this->addSql('DROP INDEX IDX_C96F581B1ADAD7EB ON ward');
        $this->addSql('DROP INDEX IDX_C96F581B8422C020 ON ward');
        $this->addSql('ALTER TABLE ward DROP medic, DROP patient');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet ADD medic INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cabinet ADD CONSTRAINT FK_4CED05B08422C020 FOREIGN KEY (medic) REFERENCES medic (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_4CED05B08422C020 ON cabinet (medic)');
        $this->addSql('ALTER TABLE laboratory ADD patient INT DEFAULT NULL');
        $this->addSql('ALTER TABLE laboratory ADD CONSTRAINT FK_FDC719A81ADAD7EB FOREIGN KEY (patient) REFERENCES patient (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_FDC719A81ADAD7EB ON laboratory (patient)');
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C020C96F581B');
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C0204CED05B0');
        $this->addSql('DROP INDEX IDX_8422C020C96F581B ON medic');
        $this->addSql('DROP INDEX IDX_8422C0204CED05B0 ON medic');
        $this->addSql('ALTER TABLE medic DROP ward, DROP cabinet');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBC96F581B');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB4CED05B0');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBFDC719A8');
        $this->addSql('DROP INDEX IDX_1ADAD7EBC96F581B ON patient');
        $this->addSql('DROP INDEX IDX_1ADAD7EB4CED05B0 ON patient');
        $this->addSql('DROP INDEX IDX_1ADAD7EBFDC719A8 ON patient');
        $this->addSql('ALTER TABLE patient DROP ward, DROP cabinet, DROP laboratory');
        $this->addSql('ALTER TABLE ward ADD medic INT DEFAULT NULL, ADD patient INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ward ADD CONSTRAINT FK_C96F581B1ADAD7EB FOREIGN KEY (patient) REFERENCES patient (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE ward ADD CONSTRAINT FK_C96F581B8422C020 FOREIGN KEY (medic) REFERENCES medic (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C96F581B1ADAD7EB ON ward (patient)');
        $this->addSql('CREATE INDEX IDX_C96F581B8422C020 ON ward (medic)');
    }
}
