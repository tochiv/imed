<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505080120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C0208D93D649');
        $this->addSql('DROP INDEX IDX_8422C0208D93D649 ON medic');
        $this->addSql('ALTER TABLE medic DROP user');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB8D93D649');
        $this->addSql('DROP INDEX IDX_1ADAD7EB8D93D649 ON patient');
        $this->addSql('ALTER TABLE patient DROP user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE medic ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C0208D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8422C0208D93D649 ON medic (user)');
        $this->addSql('ALTER TABLE patient ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB8D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1ADAD7EB8D93D649 ON patient (user)');
    }
}
