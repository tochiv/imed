<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505092547 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet DROP FOREIGN KEY FK_4CED05B08D93D649');
        $this->addSql('DROP INDEX IDX_4CED05B08D93D649 ON cabinet');
        $this->addSql('ALTER TABLE cabinet DROP user');
        $this->addSql('ALTER TABLE laboratory DROP FOREIGN KEY FK_FDC719A88D93D649');
        $this->addSql('DROP INDEX IDX_FDC719A88D93D649 ON laboratory');
        $this->addSql('ALTER TABLE laboratory DROP user');
        $this->addSql('ALTER TABLE ward DROP FOREIGN KEY FK_C96F581B8D93D649');
        $this->addSql('DROP INDEX IDX_C96F581B8D93D649 ON ward');
        $this->addSql('ALTER TABLE ward DROP user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cabinet ADD CONSTRAINT FK_4CED05B08D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_4CED05B08D93D649 ON cabinet (user)');
        $this->addSql('ALTER TABLE laboratory ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE laboratory ADD CONSTRAINT FK_FDC719A88D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_FDC719A88D93D649 ON laboratory (user)');
        $this->addSql('ALTER TABLE ward ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ward ADD CONSTRAINT FK_C96F581B8D93D649 FOREIGN KEY (user) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C96F581B8D93D649 ON ward (user)');
    }
}
