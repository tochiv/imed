<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505092346 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet ADD medic INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cabinet ADD CONSTRAINT FK_4CED05B08422C020 FOREIGN KEY (medic) REFERENCES medic (id)');
        $this->addSql('CREATE INDEX IDX_4CED05B08422C020 ON cabinet (medic)');
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C0204CED05B0');
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C020C96F581B');
        $this->addSql('DROP INDEX IDX_8422C0204CED05B0 ON medic');
        $this->addSql('DROP INDEX IDX_8422C020C96F581B ON medic');
        $this->addSql('ALTER TABLE medic ADD user INT DEFAULT NULL, DROP ward, DROP cabinet');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C0208D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8422C0208D93D649 ON medic (user)');
        $this->addSql('ALTER TABLE patient ADD user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EB8D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1ADAD7EB8D93D649 ON patient (user)');
        $this->addSql('ALTER TABLE ward ADD medic INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ward ADD CONSTRAINT FK_C96F581B8422C020 FOREIGN KEY (medic) REFERENCES medic (id)');
        $this->addSql('CREATE INDEX IDX_C96F581B8422C020 ON ward (medic)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cabinet DROP FOREIGN KEY FK_4CED05B08422C020');
        $this->addSql('DROP INDEX IDX_4CED05B08422C020 ON cabinet');
        $this->addSql('ALTER TABLE cabinet DROP medic');
        $this->addSql('ALTER TABLE medic DROP FOREIGN KEY FK_8422C0208D93D649');
        $this->addSql('DROP INDEX IDX_8422C0208D93D649 ON medic');
        $this->addSql('ALTER TABLE medic ADD cabinet INT DEFAULT NULL, CHANGE user ward INT DEFAULT NULL');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C0204CED05B0 FOREIGN KEY (cabinet) REFERENCES cabinet (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE medic ADD CONSTRAINT FK_8422C020C96F581B FOREIGN KEY (ward) REFERENCES ward (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8422C0204CED05B0 ON medic (cabinet)');
        $this->addSql('CREATE INDEX IDX_8422C020C96F581B ON medic (ward)');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EB8D93D649');
        $this->addSql('DROP INDEX IDX_1ADAD7EB8D93D649 ON patient');
        $this->addSql('ALTER TABLE patient DROP user');
        $this->addSql('ALTER TABLE ward DROP FOREIGN KEY FK_C96F581B8422C020');
        $this->addSql('DROP INDEX IDX_C96F581B8422C020 ON ward');
        $this->addSql('ALTER TABLE ward DROP medic');
    }
}
