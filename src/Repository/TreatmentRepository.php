<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Treatment;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Treatment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Treatment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Treatment[]    findAll()
 * @method Treatment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TreatmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Treatment::class);
    }

    public function getTreatments(User $user)
    {
        $qb = $this->createQueryBuilder('t');
        $expr = $qb->expr();

        return $qb
            ->addSelect('t')
            ->innerJoin('t.medic', 'm')
            ->andWhere($expr->eq('m.user', ':user'))
            ->innerJoin('t.patient', 'p')
            ->andWhere($expr->eq('p.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
