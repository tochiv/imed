<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Medic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Medic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Medic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Medic[]    findAll()
 * @method Medic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Medic::class);
    }
}
