<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Cabinet;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cabinet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cabinet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cabinet[]    findAll()
 * @method Cabinet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CabinetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cabinet::class);
    }

    public function getMedics(User $user)
    {
        $qb = $this->createQueryBuilder('c');
        $expr = $qb->expr();

        return $qb
            ->addSelect('c')
            ->innerJoin('c.medic', 'm')
            ->andWhere($expr->eq('m.user', ':user'))
            ->innerJoin('c.patient', 'p')
            ->andWhere($expr->eq('p.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
