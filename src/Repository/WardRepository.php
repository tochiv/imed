<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use App\Entity\Ward;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ward|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ward|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ward[]    findAll()
 * @method Ward[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ward::class);
    }

    public function getWards(User $user)
    {
        $qb = $this->createQueryBuilder('w');
        $expr = $qb->expr();

        return $qb
            ->addSelect('w')
            ->innerJoin('w.medic', 'm')
            ->andWhere($expr->eq('m.user', ':user'))
            ->innerJoin('w.patient', 'p')
            ->andWhere($expr->eq('p.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
