<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Laboratory;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Laboratory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Laboratory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Laboratory[]    findAll()
 * @method Laboratory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LaboratoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Laboratory::class);
    }

    public function getLaboratory(User $user)
    {
        $qb = $this->createQueryBuilder('l');
        $expr = $qb->expr();

        return $qb
            ->addSelect('l')
            ->innerJoin('l.patient', 'p')
            ->andWhere($expr->eq('p.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
