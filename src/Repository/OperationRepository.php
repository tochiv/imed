<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Operation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }

    public function getOperations(User $user)
    {
        $qb = $this->createQueryBuilder('o');
        $expr = $qb->expr();

        return $qb
            ->addSelect('o')
            ->innerJoin('o.medic', 'm')
            ->andWhere($expr->eq('m.user', ':user'))
            ->innerJoin('o.patient', 'p')
            ->andWhere($expr->eq('p.user', ':user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
